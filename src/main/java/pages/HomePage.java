package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import manager.FileReaderManager;

public class HomePage {
	WebDriver driver;
	 
	 public HomePage(WebDriver driver) {
	 this.driver = driver;
	 PageFactory.initElements(driver, this);
	 }
	 
	 public void perform_Search(String search) {
	 driver.navigate().to(FileReaderManager.getFileReaderManager().getConfigFileReader().getApplicationURL() +"/?s=" + search + "&post_type=product");
	 }
	 
	 public void navigateTo_HomePage() {
	 driver.get(FileReaderManager.getFileReaderManager().getConfigFileReader().getApplicationURL());
	 }

}
