package cucumber;

import manager.PageObjectManager;
import manager.WebDriverManager;

public class TestContext {
	private WebDriverManager webDriverManager;
	private PageObjectManager pageObjectManager;
	public ScenarioContext scenarioContext;
	
	public TestContext(WebDriverManager webDriverManager, PageObjectManager pageObjectManager) {
		super();
		this.webDriverManager = webDriverManager;
		this.pageObjectManager = new PageObjectManager(webDriverManager.getDriver());
		this.scenarioContext=new ScenarioContext();
	}

	public WebDriverManager getWebDriverManager() {
		return webDriverManager;
	}

	public void setWebDriverManager(WebDriverManager webDriverManager) {
		this.webDriverManager = webDriverManager;
	}

	public PageObjectManager getPageObjectManager() {
		return pageObjectManager;
	}

	public void setPageObjectManager(PageObjectManager pageObjectManager) {
		this.pageObjectManager = pageObjectManager;
	}
	
	public ScenarioContext getScenarioContext() {
		return scenarioContext;
	}

}
