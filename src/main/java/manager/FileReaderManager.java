package manager;

import dataProvider.ConfigFileReader;

public class FileReaderManager {
	
	private static FileReaderManager fileReaderManager;
	private static ConfigFileReader configFileReader;

	private FileReaderManager() {
		
	}
	
	public static FileReaderManager getFileReaderManager() {
		return fileReaderManager;
	}
	
	public ConfigFileReader getConfigFileReader() {
		return (configFileReader==null)?new ConfigFileReader():configFileReader;
	}
}
