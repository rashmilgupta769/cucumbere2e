package dataProvider;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import enums.DriverType;
import enums.EnvironmentType;
import net.bytebuddy.implementation.bytecode.Throw;

public class ConfigFileReader {
	private Properties prop;
	private final String propertFile="config//config.properties";
	

	
	public ConfigFileReader() {
		File file=new File(propertFile) ;
		prop=new Properties();
		FileInputStream fis;
		try {
			fis = new FileInputStream(file);
			prop.load(fis);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	
	public String getDriverPath() {
		String driverPath=prop.getProperty("driverPath");
		return driverPath;
	}
	public String getApplicationURL() {
		String url=prop.getProperty("url");
		return url;
	}
	
	public long getImplicitlyWait() {
		String implicitWait=prop.getProperty("implicitWait");
		return Long.parseLong(implicitWait);
	}
	
	public DriverType getBrowser() {
		String browser=prop.getProperty("browser");
		if(browser==null || browser.equals("chrome")) {
			return DriverType.CHROME;
		}else if (browser.equals("ie")) {
			return DriverType.INTERNETEXPLORER;
			
		}else {
			 throw new RuntimeException("Browser key not present");
		}
	}
	public EnvironmentType getEnvironment() {
		String env=prop.getProperty("environment");
		if(env.equals("local")) {
			return EnvironmentType.LOCAL;
		}else {
			return EnvironmentType.REMOTE;
		}
	}
	
	
}
