package com.epam.project.Cucumber.Runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/features",
glue="com.epam.project.Cucumber.steps",
monochrome = true,
dryRun = false,
plugin= {"pretty", "html:target/htmlReport", "json:target/cucumber.json", "junit:target/cucumber.xml"})
public class RunCucumberTest {

}
